--[[
Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
Author: ZwerOxotnik
Version: 1.1.4 (2019-04-24)

Description: This mod for mod developers and scenarios. Adds fake tiles.
			 Players can't build anything on the tiles but can walk on the tiles

You can write and receive any information on the links below.
Source: https://github.com/ZwerOxotnik/fake-tiles
Mod portal: https://mods.factorio.com/mod/fake-tiles
]]

local fake_collision_mask = {
	"water-tile",
	"ground-tile",
	"item-layer",
	"resource-layer",
	"object-layer"
}

local new_tiles = {}
for _, prototype in pairs(data.raw.tile) do
	if prototype.collision_mask ~= fake_collision_mask then
		local new_tile = util.table.deepcopy(prototype)
		new_tile.name = "fakeB-" .. new_tile.name
		new_tile.collision_mask = fake_collision_mask
		new_tile.autoplace = nil
		new_tile.localised_name = {"", {"tile-name." .. prototype.name}, " [fake]"}
		table.insert(new_tiles, new_tile)
	end
end
data:extend(new_tiles)
